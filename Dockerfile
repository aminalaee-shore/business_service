FROM python:3.9-alpine AS builder
COPY requirements.txt .
RUN pip install -r requirements.txt


FROM python:3.9-alpine
ENV PYTHONUNBUFFERED=1 PYTHONDONTWRITEBYTECODE=1
COPY --from=builder /usr/local/lib/ /usr/local/lib/
WORKDIR /app
COPY . .
CMD ["python", "-m", "uvicorn", "main:app", "--host", "0.0.0.0"]
