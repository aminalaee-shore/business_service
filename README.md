## Project Structure
The project is a simple consumer of events in the `api` project. It doesn't implement any databases for the sake of simplicity and stores all the data in memory.

Consuming product prices from `api` and implement sending an email when new we see product again; meaning price has changed.

The swagger is automatically generated at `/docs`.
