from typing import List, Optional

from pydantic import BaseModel


class Product(BaseModel):
    id: int
    category: str
    name: str
    sku: str
