from fastapi import FastAPI
import os
import json
from walrus import Database
from starlette.background import BackgroundTask

app = FastAPI()
app.state.datastore = {}

db = Database(os.environ.get("REDIS_HOST", "localhost"))
stream = db.Stream("PRODUCTS")


def process_events():
    while True:
        events = stream.read(block=5000, last_id='$')
        for event in events:
            product = json.loads(event)

            if product['name'] in app.state.datastore:
                continue #  Send email

            app.state.datastore[product['name']] = product['sku']


@app.get("/")
def read_index():
    return app.state.datastore


BackgroundTask(process_events)
